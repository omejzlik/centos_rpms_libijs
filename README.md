# libijs

The IJS (InkJet Server) Raster Image Transport Protocol is a library, which is no longer actively developed, and often other alternatives are used instead.

This library, however, still seem to be useful for Ghostscript application to be able to connect to the HP IJS server to print on an HP printer.
